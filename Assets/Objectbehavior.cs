﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectBehavior : MonoBehaviour {

    private int currentKeyGlobal;
    private Vector3 speed;
    private List<Vector3> waypoints;
    private int objectID;
    private float counter;
    private Vector3 position;
    private GameObject point;
    private float maxSpeed;
    private float frametime;
    private float initialWaypointDistance;
    private float maxAcceleration;
    private bool goalReached;
    float DistanceTraveled = 0;
	private int numberOfVehicles;
	private bool foundEveryone;
	private bool formationPointsSet;
	private bool readyToAttack;
	private float visibilityRange = 150.0f;
	private GameObject[] restOfTheVehicles;
	private List<vehicleInfo> vehiclesInformation = new List<vehicleInfo>();
	float vehicleRadius;
	private float counterTime;

    // Use this for initialization
	public ObjectBehavior(Vector3 startPosition, List<Vector3> _waypoints, int ID,int _numberOfVehicles = 0)
    {
		foundEveryone = false;
		formationPointsSet = false;
		readyToAttack = false;
		numberOfVehicles = _numberOfVehicles;
        position = startPosition;
        waypoints = _waypoints;
        objectID = ID;
        counter = 0;
        point = GameObject.Find("Vehicle" + objectID.ToString());
        maxSpeed = 10;
        speed = new Vector3(0, 0, 0);
        currentKeyGlobal = 0;
        counter = 0;
        maxAcceleration = 1;
        goalReached = false;
		vehicleInfo temp=new vehicleInfo();
		temp.position=startPosition;
		temp.speed=speed;
		vehicleRadius = 1;
		counterTime = 0;
		for (int i = 0; i < objectID; i++)
		{
			vehiclesInformation.Add(temp);
		}

        initialWaypointDistance = checkDistanceToWaypoint();
    }

	public Vector3 getSpeed()
	{
		return speed;
	}
	public Vector3 getPosition()
	{
		return position;
	}

	public struct vehicleInfo
	{
		public Vector3 position;
		public Vector3 speed;
	}

	public void setVehicleInformation(Vector3 speedID, Vector3 positionID, int ID)
	{
		vehicleInfo tempa = new vehicleInfo();
		tempa.speed = speedID;
		tempa.position = positionID;
		vehiclesInformation[ID] = tempa;

	}

	public void Move_1_2() {
		frametime = 1 / Time.deltaTime;
		position = point.transform.position;
		Vector3 desiredChange = DynamicPoint ();
		speed = speed + desiredChange;
		if (speed.magnitude > maxSpeed)
			speed = maxSpeed * speed / speed.magnitude;
		if (goalReached)
			speed = new Vector3 (0, 0, 0);
		point.transform.position += speed / 50;
		DistanceTraveled += speed.magnitude / 50;
		float distance = checkDistanceToWaypoint();
		if (distance < 2) {
			currentKeyGlobal++;
			if (currentKeyGlobal != waypoints.Count) {
				initialWaypointDistance = checkDistanceToWaypoint ();
			}
		}
		if (currentKeyGlobal == waypoints.Count) {
			currentKeyGlobal = currentKeyGlobal - 1;
			if (speed.magnitude < distance / frametime) {
				goalReached = true;
			}
		}
	}

    // Update is called once per frame
	public float[][] Move(float[][] map)
	{

		if (goalReached == true)
		{
			map = updateMap(map, Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.y), -2);

			return map;
		}
		counterTime++;

		//Debug.Log(counterTime / 50);
		frametime = 1 / Time.deltaTime;

		position = point.transform.position;

		Vector3 desiredChange = DynamicPoint();
		map = updateMap(map, Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.y), 0);
		//map[Mathf.RoundToInt(position.x)][Mathf.RoundToInt(position.y)] = false;
		Vector3 speedChange = localObjectAvoidance2(desiredChange,map);
		//speedChange = dynamicObstacleAvoidance(speedChange);
		//Debug.Log(position+ " change: " + desiredChange);

		if (float.IsNaN(speedChange.x))
			speed = new Vector3(0, 0, 0);
		else
			speed = speed + speedChange;
		if (speed.magnitude > maxSpeed)
			speed = maxSpeed * speed / speed.magnitude;

		if (goalReached == true)
			speed = new Vector3(0, 0, 0);



		//Debug.Log("x: " +position.x + " y: " +Mathf.RoundToInt(position.y));


		point.transform.position = position + speed/50;

		map = updateMap(map, Mathf.RoundToInt((position + speed/50).x), Mathf.RoundToInt((position + speed/50).y), -1);
		//map[Mathf.RoundToInt((position + speed/50).x)][Mathf.RoundToInt((position + speed/50).y)] = true;

		//DistanceTraveled = DistanceTraveled + speed.magnitude/50;

		position = point.transform.position;
		// here we will set its old position to "true" and its new position to "false"
		//Debug.Log(speed.magnitude + " distaceTrav: " + DistanceTraveled+ " speedchangemag: " + speedChange.magnitude);



		float distance = checkDistanceToWaypoint();

		if (distance<vehicleRadius )
		{
			currentKeyGlobal++;
			if (currentKeyGlobal != waypoints.Count)
				initialWaypointDistance = checkDistanceToWaypoint();
		}

		if (currentKeyGlobal==waypoints.Count)
		{
			currentKeyGlobal = currentKeyGlobal - 1;
			if (speed.magnitude < maxAcceleration/50)
			{
				goalReached = true;
				speed = new Vector3(0, 0, 0);

			}
		}        


		return map;

	}

	private float[][] updateMap(float[][] map, int x, int y, float value)
	{
		int radius = (int)vehicleRadius - 1;

		int deltaX = 0;
		int deltaY = 0;

		if (radius == 0)
			map[x][y] = value;
		else
		{
			for (int i = 0; i <= radius; i++)
			{
				deltaY = Mathf.RoundToInt(Mathf.Sqrt(radius * radius - i * i));


				for (int j = -deltaY; j <= deltaY; j++)
				{


					//Debug.Log(x + " deltax: " + deltaX + "y:" + y + " deltaY: " + deltaY +" radius: " + radius);
					map[x + i][y + j] = value;
					map[x - i][y + j] = value;

				}
			}
		}
		/*
        GameObject obsPoint = GameObject.Find("obstaclePoint");
        for (int i = x-10; i < x+10; i++)
        {
            for (int j =y-10; j < y+10; j++)
            {
                Vector3 test = new Vector3(i, j, 0);
                if (map[i][j] <0)
                    Instantiate(obsPoint, test, obsPoint.transform.rotation);
            }
        }
        */
		return map;
	}

	public float checkDistanceToWaypoint()
	{
		Vector3 goal = (Vector3)waypoints[currentKeyGlobal];

		float distance = (goal - position).magnitude;
		return distance;
	}

    public Vector3 DynamicPoint()
    {
        Vector3 goal = (Vector3)waypoints[currentKeyGlobal];
        Vector3 direction = goal - position;

        float Acceleration = 1 * maxAcceleration;// * accMultiplier;
        float DeAcceleration = 1 * maxAcceleration;// * accMultiplier;
        Vector3 change;
        float angle;
        if (speed == new Vector3(0, 0, 0))
        {

            change = (direction / direction.magnitude) * Acceleration;
        }
        else
        {
            angle = Vector3.Angle(direction / direction.magnitude, -speed / speed.magnitude);




            if (angle < 177)
            {
                Acceleration = 0.9f * maxAcceleration;
                change = (direction / direction.magnitude) * Acceleration - (speed / speed.magnitude) * DeAcceleration;
                
            }
            else
            {

                float nFrames = direction.magnitude / speed.magnitude;
                float nAccNecesary = Acceleration * nFrames;



                if (speed.magnitude > nAccNecesary)
                    Acceleration = -Acceleration;


                change = (direction / direction.magnitude) * Acceleration;

            }

        }
        // calculating the accelerations direction and magnitude
        
        
        if (change.magnitude > maxAcceleration)
            change = maxAcceleration * change / change.magnitude;
        change.x = change.x / frametime; 
        change.y = change.y / frametime;

        if (goalReached == true)
            change = new Vector3(0, 0, 0);

        return change;

    }


    private Vector3 localObjectAvoidance2(Vector3 desiredChange, float[][] map)
	{
		Vector3 goal = (Vector3)waypoints[currentKeyGlobal];
		Vector3 directionOfGoal = goal - position;

		float distanceToGoal = directionOfGoal.magnitude;



		int distanceToCheck = 10 + (int) vehicleRadius; //in pixels

		if (distanceToGoal<distanceToCheck)        
			distanceToCheck = (int)distanceToGoal;

		Vector3 directionOfSpeed = speed / speed.magnitude;
		Vector3 perDirectionOfSpeed = new Vector3(-directionOfSpeed.y, directionOfSpeed.x, 0);

		if (speed == new Vector3(0, 0, 0))
			directionOfSpeed = new Vector3(0, 0, 0);

		//GameObject obsPoint = GameObject.Find("obstaclePoint");
		//obsPoint.transform.localScale = new Vector3(1, 1, 1);
		bool obstacleZero = false;

		bool obstacleMinus = false;
		bool obstaclePlus = false;
		int minusIndex = int.MaxValue;
		int plusIndex = int.MaxValue;
		for (int i =(int)vehicleRadius;i<distanceToCheck;i++)
		{
			for (int k = -1; k < 1; k++)
			{
				int x = Mathf.RoundToInt(position.x + i * directionOfSpeed.x + vehicleRadius * k * perDirectionOfSpeed.x);
				int y = Mathf.RoundToInt(position.y + i * directionOfSpeed.y + vehicleRadius * k * perDirectionOfSpeed.y);
				if (x > 0 && x < 300 && y > 0 && y < 300)
				{
					if (map[x][y] < -1)
					{
						//obsPoint.transform.position = new Vector3(Mathf.RoundToInt(position.x + i  directionOfSpeed.x), Mathf.RoundToInt(position.y + i  directionOfSpeed.y), 0);

						if (k == -1 && obstacleMinus == false)
						{
							minusIndex = i;
							obstacleMinus = true;
						}
						if (k == 1 && obstaclePlus == false)
						{
							obstaclePlus = true;
							plusIndex = i;
						}
						if (k == 0)
						{
							obstacleZero = true;
						}

					}
				}
			}


		}
		//Debug.Log(obstacle);
		Vector3 newChange = desiredChange;
		if (obstacleMinus == true && obstaclePlus == true)
		{
			if (plusIndex < minusIndex)
			{ //turn away from plus index
				newChange = perDirectionOfSpeed * desiredChange.magnitude + desiredChange;
			}
			else
			{//turn away from minus index
				newChange = -perDirectionOfSpeed * desiredChange.magnitude + desiredChange;
			}

		}
		else if (obstacleMinus == true)
		{//turn away from obstacle minus
			newChange = -perDirectionOfSpeed * desiredChange.magnitude + desiredChange;
		}
		else if (obstaclePlus == true)
		{// turn away from obstacle plus
			newChange = perDirectionOfSpeed * desiredChange.magnitude + desiredChange;
		}
		else if (obstacleZero == true)
		{
			newChange = directionOfSpeed * desiredChange.magnitude + desiredChange;
		}


		if (newChange.magnitude > (maxAcceleration / frametime))
		{
			//Debug.Log("changing because above max acc" + newChange.magnitude + " acc max: " + maxAcceleration / frametime);
			newChange = maxAcceleration * newChange / (newChange.magnitude * frametime);

		}

		//Debug.Log(newChange.magnitude + " newChange" + distanceToGoal);

		return newChange;
	}

	private Vector3 dynamicObstacleAvoidance(Vector3 desiredChange)
	{

		if (objectID == 0)
			return desiredChange;




		Vector3 zero = new Vector3(0, 0, 0);

		int iter = (int)(speed.magnitude / (maxAcceleration * 0.02f));
		if (speed == zero)
			iter = 2;



		Vector3[] Va = new Vector3[iter+2];

		for (int i = 0; i < iter+2; i++)
		{
			Va[i] = position + speed * (i + 1);
		}


		Vector3[,] Vb = new Vector3[objectID, iter+2];

		bool collision = false;
		//Debug.Log(iter);
		for (int i = 0; i < objectID; i++)
		{
			for (int k = 0; k < iter+2; k++)
			{
				Vb[i,k] = vehiclesInformation[i].position + (k+1)*vehiclesInformation[i].speed;
				if (Vb[i, k].magnitude > 100000)
					Debug.Log(Vb[i, k] + " pos: " + vehiclesInformation[i].position + " speed: " + vehiclesInformation[i].speed + " k: " +k );
				if ((Va[k] - Vb[i,k]).magnitude < vehicleRadius * 2)
					collision = true;

			}            
		}

		Vector3 newChange=desiredChange;
		if (collision == true && speed!=zero) 
			newChange = -0.02f*(speed / speed.magnitude);

		return newChange;

	}


    private Vector3 localObjectAvoidance(Vector3 desiredChange, bool[][] map)
    {
        /*
        Vector3 firstPos = position;
        Vector3 secondPos = position + desiredChange;
        Vector3 thirdPos = position + speed;
        Vector3 fourthPos = position + speed + desiredChange;
        */

        Vector3 goal = (Vector3)waypoints[currentKeyGlobal];
        Vector3 direction = goal - position;

        float nFrames = direction.magnitude / (speed.magnitude*frametime);

        if (speed.magnitude == 0)
            nFrames = 0;

        //Debug.Log(nFrames);
        
        //Debug.Log("speed: " + speed.magnitude + " frametime: " + frametime + " desiredChange: " + desiredChange.x );
        Vector3[] cornerPos = new Vector3[4];
        cornerPos[0].x = ( position.x);
        cornerPos[0].y = (position.y);

        cornerPos[1].x = (position.x+ desiredChange.x);
        cornerPos[1].y = (position.y+desiredChange.y);

        cornerPos[2].x = (position.x+speed.x);
        cornerPos[2].y = (position.y+speed.y);

        cornerPos[3].x = (position.x+(speed.x+desiredChange.x));
        cornerPos[3].y = (position.y+(speed.y + desiredChange.y));


        int iter =10;

        //Debug.Log("iter: " + iter + " nframes: " + frametime + " iterChange: " + iterChange);

        float speedIter= speed.magnitude * 50;
        float changeIter = desiredChange.magnitude * 50;

        float speedChangeQuote = (cornerPos[1] - cornerPos[0]).magnitude / (cornerPos[2] - cornerPos[0]).magnitude;
        Debug.Log(speedIter+ " speed , change: " + changeIter);

        Vector3 speedDirection = (cornerPos[2] - cornerPos[0])/ (cornerPos[2] - cornerPos[0]).magnitude;
        Vector3 changeDirection = (cornerPos[1] - cornerPos[0])/ (cornerPos[1] - cornerPos[0]).magnitude;


        //Debug.Log(speedDirection.magnitude + " speed mag" +  speed);

        List<Vector3> obstaclesList = new List<Vector3>();

        int xlength = map.Length;
        int ylength = map[0].Length;

        GameObject obsPoint = GameObject.Find("obstaclePoint");

        for (int i =0;i<iter;i++)
        {
            for(int j=0;j<iter;j++)
            {
                int x = Mathf.RoundToInt(cornerPos[0].x + speedDirection.x * i + changeDirection.x * j);
                int y = Mathf.RoundToInt(cornerPos[0].y + speedDirection.y * i + changeDirection.y * j);
                
                if (x < xlength && x >= 0 && y < ylength && y >= 0)
                {
                    if (map[x][y] == true)
                    {
                        Vector3 obstacle = new Vector3(x, y, 0);
                        //Instantiate(obsPoint, obstacle, obsPoint.transform.rotation);
                        obstaclesList.Add(obstacle);
                        //Debug.Log("objectFound");
                    }
                }

            }
        }
        
        /* // paints all the obstacles
        for (int i=0;i<xlength;i++)
        {
            for(int j=0;j<ylength;j++)
            {
                Vector3 test = new Vector3(i, j, 0);
                if (map[i][j] == true)
                    Instantiate(obsPoint, test, obsPoint.transform.rotation);
            }
        }
        */
        Vector3 objectsForce = new Vector3(0, 0, 0);

        Vector3 difference;
        for (int i = 0; i < obstaclesList.Count; i++)
        {
            difference= cornerPos[3] - obstaclesList[i];
            objectsForce = objectsForce - difference/(difference.magnitude * difference.magnitude);
        }
        

        Vector3 positionChange;

        if (objectsForce!=new Vector3(0,0,0))
        {
            objectsForce = maxAcceleration * objectsForce / objectsForce.magnitude;
            positionChange = desiredChange + objectsForce/frametime;
            positionChange = maxAcceleration * positionChange / positionChange.magnitude;
            //Debug.Log("Object found in the way new positionchange: "+ positionChange);
        }
        else
        {
            positionChange = desiredChange;
        }
        

        

        return positionChange;

    }

}

