﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class mapCreation : MonoBehaviour {
	float[] polygonX, polygonY;
	int[] button;
	Vector3[] startPoints, goalPoints, customerPoints;
	int width, height;
	private bool[][] map;
	private bool[] pathFound;
	private List<Vector3>[] tasksAssigned;
	private List<Vector3>[] plannedPath;
	private List<Vector3>[] plannedPathFiltered;
	private float[][] floatMap;
	private List<vehicleInfo> vehiclesInformation = new List<vehicleInfo>();

	private List<ObjectBehavior> vehicles= new List<ObjectBehavior>();
	private vehicleInfo tempo;

	public struct Edge {
		public float x1, y1, x2, y2;
		public Edge(float x1, float y1, float x2, float y2) {
			this.x1 = x1;
			this.y1 = y1;
			this.x2 = x2;
			this.y2 = y2;
		}
	}

	Edge[] edges;

	void buildEdgeArray() {
		ArrayList temp = new ArrayList();
		for (int i = 0; i < button.Length; i++) {
			int start = i;
			while (button [i] == 1) {
				temp.Add (new Edge (polygonX [i], polygonY [i], polygonX [i + 1], polygonY [i + 1]));
				i++;
			}
			temp.Add (new Edge (polygonX [i], polygonY [i], polygonX [start], polygonY [start]));
		}
		edges = new Edge[temp.Count];
		for (int j = 0; j < temp.Count; j++) {
			Edge e = (Edge)temp[j];
			edges[j] = e;
		}
	}

	//A* for eight movement directions
	public List<Vector3> Neighbour8(bool[][] grid, Vector3 start, Vector3 stop) {
		int height = grid[0].Length;
		int width = grid.Length;

		int[,] G;
		int[,] F;
		ArrayList open = new ArrayList ();
		ArrayList closed = new ArrayList ();
		Vector3[,] parent;

		G = new int[width, height];
		F = new int[width, height];
		parent = new Vector3[width, height];

		//Set the starting values based on the start point
		open.Add(start);
		G[Mathf.RoundToInt (start.x), Mathf.RoundToInt(start.y)] = 0;
		F[Mathf.RoundToInt(start.x), Mathf.RoundToInt(start.y)] = HeuristicCost(start, stop);
		parent[Mathf.RoundToInt(start.x), Mathf.RoundToInt(start.y)] = start;

		//Find the currently cheapest node to move to
		while (open.Count > 0) {
			Vector3 currentNode = (Vector3)open [0];
			for (int i = 1; i < open.Count; i++) {
				Vector3 compareNode = (Vector3)open [i];
				if (F [Mathf.RoundToInt(currentNode.x), Mathf.RoundToInt(currentNode.y)] >= F [Mathf.RoundToInt(compareNode.x), Mathf.RoundToInt(compareNode.y)]) {
					currentNode = compareNode;
				}
			}

			//Once a path is found, reconstruct the path backwards, from the stop to the start, then reverse the result and return it
			if (currentNode.x == stop.x && currentNode.y == stop.y) {
				List<Vector3> path = new List<Vector3>();
				while (!(parent[Mathf.RoundToInt(stop.x), Mathf.RoundToInt(stop.y)].x == stop.x && parent[Mathf.RoundToInt(stop.x), Mathf.RoundToInt(stop.y)].y == stop.y))
				{
					path.Add(stop);
					stop = parent[Mathf.RoundToInt(stop.x), Mathf.RoundToInt(stop.y)];
				}
				path.Reverse();
				return path;
			}

			//Since we are now examining the current node, it goes to the closed heap to prevent it from being examined again
			closed.Add (currentNode);
			open.Remove (currentNode);

			//Now go through all 8 surrounding nodes and add those 
			for (int x = Mathf.RoundToInt(currentNode.x) - 1; x <= Mathf.RoundToInt(currentNode.x) + 1; x++) {
				for (int y = Mathf.RoundToInt(currentNode.y) - 1; y <= Mathf.RoundToInt(currentNode.y) + 1; y++) {
					//Check grid boundaries
					if (x >= 0 && x < width) {
						if (y >= 0 && y < height) {
							//Check that a point is passable and not already examined
							if (grid [x] [y] == false && closed.IndexOf (new Vector3 (x, y, 0.0f)) == -1) {
								//When going diagonal, make sure we are not going through walls
								if (!(x != Mathf.RoundToInt(currentNode.x) && y != Mathf.RoundToInt(currentNode.y)) || (!grid [Mathf.RoundToInt(currentNode.x)] [y] && !grid [x] [Mathf.RoundToInt(currentNode.y)])) {
									//If the point has not been considered yet, add it to the open list and calculate G,F and parent
									if (open.IndexOf (new Vector3 (x, y, 0.0f)) == -1) {
										if (x != currentNode.x && y != currentNode.y)
											G [x, y] = G [Mathf.RoundToInt(currentNode.x), Mathf.RoundToInt(currentNode.y)] + 14;
										else
											G [x, y] = G [Mathf.RoundToInt(currentNode.x), Mathf.RoundToInt(currentNode.y)] + 10;

										F [x, y] = G [x, y] + HeuristicCost (new Vector3 (x, y, 0.0f), stop);
										parent [x, y] = currentNode;
										open.Add (new Vector3 (x, y, 0.0f));
									}
									//If the point has been previously considered, update G,F and parent only if the new path is faster
									else {
										if (x != currentNode.x && y != currentNode.y) {
											if (G [Mathf.RoundToInt(currentNode.x), Mathf.RoundToInt(currentNode.y)] + 14 < G [x, y]) {
												G [x, y] = G [Mathf.RoundToInt(currentNode.x), Mathf.RoundToInt(currentNode.y)] + 14;
												F [x, y] = G [x, y] + HeuristicCost (new Vector3 (x, y, 0.0f), stop);
												parent [x, y] = currentNode;
											}
										} else if (G [Mathf.RoundToInt(currentNode.x), Mathf.RoundToInt(currentNode.y)] + 10 < G [y, x]) {
											G [x, y] = G [Mathf.RoundToInt(currentNode.x), Mathf.RoundToInt(currentNode.y)] + 10;
											F [x, y] = G [x, y] + HeuristicCost (new Vector3 (x, y, 0.0f), stop);
											parent [x, y] = currentNode;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return null;
	}

	//Manhattan distance
	int HeuristicCost(Vector3 a, Vector3 b) {
		return Mathf.RoundToInt(10.0f * (Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y)));
	}

	private void createVehicles0() {
		int nVehicles = startPoints.Length;
		GameObject vehicle = GameObject.Find("Sphere");
		for (int i = 0; i < nVehicles; i++) {
			Vector3 pos = startPoints [i]; // create the clone: 
			GameObject clone = (GameObject)Instantiate (vehicle, pos, Quaternion.identity);// name it:

			clone.transform.localScale = new Vector3 (7, 7, 7);
			clone.name = "Vehicle" + i.ToString ();
			//Debug.Log(clone.transform.position);

			ObjectBehavior temp = new ObjectBehavior (startPoints [i], plannedPathFiltered [i], i, nVehicles);

			vehicles.Add (temp);
		}
	}

	private void createVehicles() {
		int nVehicles = startPoints.Length;
		GameObject vehicle = GameObject.Find("Sphere");
		for (int i = 0; i < nVehicles; i++) {
			Vector3 pos = startPoints [i]; // create the clone: 
			GameObject clone = (GameObject)Instantiate (vehicle, pos, Quaternion.identity);// name it:
			vehicleInfo temp2=new vehicleInfo();
			Vector3 initialSpeed = new Vector3(0, 0, 0);

			clone.transform.localScale = new Vector3 (7, 7, 7);
			clone.name = "Vehicle" + i.ToString ();
			//Debug.Log(clone.transform.position);

			ObjectBehavior temp = new ObjectBehavior (startPoints [i], plannedPathFiltered [i], i);
			temp2.speed = initialSpeed;
			temp2.position = startPoints[i];
			vehiclesInformation.Add(temp2);
			vehicles.Add (temp);
		}
	}

	int costOfPath(List<Vector3> path) {
		int cost = 0;
		int i = 0;
		while (i < path.Count - 1) {
			cost += Mathf.RoundToInt (10.0f * Mathf.Sqrt (Mathf.Pow (path [i + 1].x - path [i].x, 2) + Mathf.Pow (path [i + 1].y - path [i].y, 2)));
			i++;
		}
		return cost;
	}

	int dummyCost(Vector3 start, Vector3 stop) {
		return Mathf.RoundToInt (10.0f * Mathf.Sqrt (Mathf.Pow (start.x - stop.x, 2) + Mathf.Pow (start.y - stop.y, 2)));
	}

	int[] hungryChose(int[,] costs) {
		int h = costs.GetLength(0);
		int w = costs.GetLength(1);
		bool[] rowSelected = new bool[h];
		bool[] columnSelected = new bool[w];
		int[] ret = new int[h];
		for (int i = 0; i < h; i++) {
			ret [i] = -1;
		}

		// Pick the minimum in each row and cheapest object gets picked first
		for (int repeat = 0; repeat < h; repeat++) {
			int minI = 0, minJ = 0, min = Int32.MaxValue;
			bool chosen = false;
			for (int i = 0; i < h; i++) {
				if (rowSelected [i]) {
					continue;
				}
				for (int j = 0; j < w; j++) {
					if (columnSelected [j]) {
						continue;
					}
					if (min == Math.Min (min, costs [i, j])) {
					} else {
						min = Math.Min (min, costs [i, j]);
						minI = i;
						minJ = j;
						chosen = true;
					}
				}
			}
			rowSelected [minI] = true;
			columnSelected [minJ] = true;
			if (chosen) {
				ret [minI] = minJ;
			}
		}
		return ret;
	}

	void taskAssignment() {
		Vector3[] vehiclePositions = new Vector3[startPoints.Length];
		startPoints.CopyTo (vehiclePositions, 0);
		List<Vector3> customersToBeVisited = new List<Vector3> ();
		for (int i = 0; i < customerPoints.Length; i++) {
			customersToBeVisited.Add (customerPoints [i]);
		}
		tasksAssigned = new List<Vector3>[startPoints.Length];
		for (int i = 0; i < startPoints.Length; i++) {
			tasksAssigned [i] = new List<Vector3> ();
		}
		while (customersToBeVisited.Count > 0) {
			int[,] cost = new int[vehiclePositions.Length, customersToBeVisited.Count];
			for (int i = 0; i < vehiclePositions.Length; i++) {
				for (int j = 0; j < customersToBeVisited.Count; j++) {
					//cost [i, j] = costOfPath (Neighbour8 (map, vehiclePositions [i], customersToBeVisited [j]));
					cost [i, j] = dummyCost (vehiclePositions [i], customersToBeVisited [j]);
				}
			}
			//int[] tasksChosen = HungarianAlgorithm.FindAssignments (cost);
			int[] tasksChosen = hungryChose (cost);
			for (int i = 0; i < tasksChosen.Length; i++) {
				// Debug.Log (i + "th iteration: " + tasksChosen [i]);
				if (tasksChosen [i] >= 0) {
					tasksAssigned [i].Add (customersToBeVisited [tasksChosen [i]]);
					vehiclePositions [i] = customersToBeVisited [tasksChosen [i]];
				}
			}
			// Removing chosen tasks
			if(customersToBeVisited.Count > 0)
			Array.Sort<int>(tasksChosen);
			Array.Reverse (tasksChosen);
			for (int i = 0; i < tasksChosen.Length; i++) {
				if (tasksChosen [i] >= 0) {
					Debug.Log (tasksChosen [i] + " is going to be deleted from " + customersToBeVisited.Count);
					customersToBeVisited.RemoveAt (tasksChosen [i]);
				} else {
					Debug.Log ("rejected chosen task " + tasksChosen [i]);
				}
			}
		}
		plannedPath = new List<Vector3>[startPoints.Length];
		for (int i = 0; i < startPoints.Length; i++) {
			plannedPath [i] = new List<Vector3> ();
			if (tasksAssigned [i].Count == 0) {
				plannedPath [i] = Neighbour8 (map, startPoints [i], goalPoints [i]);
			} else {
				plannedPath [i] = Neighbour8 (map, startPoints [i], tasksAssigned [i] [0]);
				for (int j = 1; j < tasksAssigned[i].Count; j++) {
					plannedPath [i].AddRange (Neighbour8 (map, tasksAssigned [i] [j - 1], tasksAssigned [i] [j]));
				}
			}
		}
		for (int i = 0; i < tasksAssigned.Length; i++) {
			for (int j = 0; j < tasksAssigned [i].Count; j++) {
				Debug.Log (i + "(" + tasksAssigned [i] [j].x + "," + tasksAssigned [i] [j].y + ")");
			}
		}
	}

	void startForTasks_3_4_5 () {
		width = 300;
		height = 300;
		polygonX = new float[] {
			34.217f,
			34.217f,
			63.94f,
			68.088f,
			84.677f,
			96.429f,
			68.779f,
			173.85f,
			166.24f,
			231.22f,
			245.05f,
			200.12f,
			237.44f,
			169.01f,
			170.39f,
			204.26f,
			204.26f,
			230.53f,
			238.82f,
			280.3f,
			77.074f,
			78.456f,
			130.3f,
			128.23f
		};
		polygonY = new float[] {
			273.25f,
			260.09f,
			254.82f,
			211.84f,
			234.65f,
			256.58f,
			275.88f,
			254.82f,
			237.28f,
			194.3f,
			216.23f,
			253.95f,
			148.68f,
			148.68f,
			56.579f,
			55.702f,
			136.4f,
			138.16f,
			103.95f,
			117.11f,
			82.895f,
			60.965f,
			58.333f,
			86.404f
		};
		button = new int[] { 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 3 };
		startPoints = new Vector3[] {
			new Vector3 (30.0f, 70.0f, 0.0f),
			new Vector3 (130.0f, 40.0f, 0.0f),
			new Vector3 (230.0f, 20.0f, 0.0f),
			new Vector3 (130.0f, 270.0f, 0.0f),
			new Vector3 (40.0f, 160.0f, 0.0f)
		};
		goalPoints = new Vector3[]{
			new Vector3 (180.0f,210.0f, 0.0f),
			new Vector3 (50.0f,230.0f, 0.0f),
			new Vector3 (20.0f,230.0f, 0.0f),
			new Vector3 (80.0f,130.0f, 0.0f),
			new Vector3 (70.0f,50.0f, 0.0f)
		};
		customerPoints = new Vector3[]{
			new Vector3 (220.0f,120.0f, 0.0f),
			new Vector3 (20.0f,120.0f, 0.0f),
			new Vector3 (220.0f,10.0f, 0.0f),
			new Vector3 (220.0f,260.0f, 0.0f),
			new Vector3 (80.0f,120.0f, 0.0f),
			new Vector3 (90.0f,220.0f, 0.0f),
			new Vector3 (120.0f,120.0f, 0.0f),
			new Vector3 (120.0f,150.0f, 0.0f)
		};

		map = new bool[width][];
		for (int i = 0; i < width; i++) {
			map[i] = new bool[height];
			for (int j = 0; j < height; j++) {
				map [i][j] = false;
			}
		}

		buildEdgeArray ();

		// Setting the camera
		GameObject camObj = GameObject.Find ("camera");
		camObj.transform.position = new Vector3 (width / 2, height / 2, -15.0f);
		Camera.main.orthographicSize = Mathf.Max (width, height) / 2;

		// Setting the background plane
		GameObject planeObj = GameObject.Find ("backgroundPlane");
		planeObj.transform.position = new Vector3 (width / 2, height / 2, 10.0f);

		// Drawing edges
		for (int i = 0; i < edges.Length; i++) {
			GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
			cube.transform.position = new Vector2(edges[i].x1, edges[i].y1);
			cube.name = "vertex" + "_" + i;
			cube.transform.localScale = new Vector3 (0.25f, 0.25f, 0.25f);
			Vector2 direction = new Vector2 (edges[i].x2 - edges[i].x1, edges[i].y2 - edges[i].y1);
			// calculate increment value based on the length of the line
			float edgelength = Mathf.Sqrt(Mathf.Pow(edges[i].x2 - edges[i].x1, 2.0f) + Mathf.Pow(edges[i].y2 - edges[i].y1, 2.0f));
			float incrementValue = 1.0f / 10.0f / edgelength;
			for (float j = 0.0f; j < 1.0f; j+=incrementValue) {
				GameObject babyCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
				babyCube.transform.position = new Vector2(edges[i].x1, edges[i].y1) + j * direction;
				babyCube.transform.localScale = new Vector3 (0.25f, 0.25f, 0.25f);
				babyCube.name = "wallUnit" + i + "_" + j;
				map [Mathf.RoundToInt (babyCube.transform.position.x)][ Mathf.RoundToInt (babyCube.transform.position.y)] = true;
				for (int x = -1; x < 2; x++) {
					for (int y = -1; y < 2; y++) {
						int X = Mathf.RoundToInt (babyCube.transform.position.x) + x;
						int Y = Mathf.RoundToInt (babyCube.transform.position.y) + y;
						if (X >= 0 && Y >= 0 && X < width && Y < height) {
							map [X] [Y] = true;
						}
					}
				}
			}
		}

		// Drawing the startPoints
		for (int i = 0; i < startPoints.Length; i++) {
			GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
			cube.transform.position = new Vector2(startPoints[i].x, startPoints[i].y);
			cube.name = "startPoint" + "_" + i;
			cube.transform.localScale = new Vector3 (2.0f, 2.0f, 2.0f);
		}

		// Drawing the customerPoints
		for (int i = 0; i < customerPoints.Length; i++) {
			GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Sphere);
			cube.transform.position = new Vector2(customerPoints[i].x, customerPoints[i].y);
			cube.name = "customerPoints" + "_" + i;
			cube.transform.localScale = new Vector3 (3.0f, 3.0f, 3.0f);
		}

		// Task assignment for all vehicles
		taskAssignment();

		// PathPlanning for all points
		// Does not consider costumers now. Needs to be modified later.
		plannedPathFiltered = new List<Vector3>[startPoints.Length];
		pathFound = new bool[startPoints.Length];
		for (int i = 0; i < startPoints.Length; i++) {
			plannedPathFiltered [i] = new List<Vector3> (plannedPath [i]);
			if (plannedPath != null) {
				Debug.Log ("Path found");
				pathFound[i] = true;
				plannedPathFiltered[i] = plannedPath[i];
				int loopVar = 0;
				while (loopVar < plannedPathFiltered [i].Count - 2) {
					// plannedPath[loopVar], plannedPath[loopVar + 1], plannedPath[loopVar + 2]
					float slope1 = (((Vector3)plannedPathFiltered [i] [loopVar + 1]).y - ((Vector3)plannedPathFiltered [i] [loopVar]).y) / (float)(((Vector3)plannedPathFiltered [i] [loopVar + 1]).x - ((Vector3)plannedPathFiltered [i] [loopVar]).x);
					float slope2 = (((Vector3)plannedPathFiltered [i] [loopVar + 2]).y - ((Vector3)plannedPathFiltered [i] [loopVar + 1]).y) / (float)(((Vector3)plannedPathFiltered [i] [loopVar + 2]).x - ((Vector3)plannedPathFiltered [i] [loopVar + 1]).x);
					if (slope1 == slope2) {
						plannedPathFiltered [i].RemoveAt (loopVar + 1);
					} else {
						loopVar++;
					}
				}
			} else {
				Debug.Log ("No path found");
				pathFound[i] = false;
			}
		}

		createVehicles();
		generateFloatMap ();
	}

	void startForTasks_1_2_A () {
		width = 300;
		height = 300;
		startPoints = new Vector3[] {
			new Vector3 (30.0f, 70.0f, 0.0f),
			new Vector3 (130.0f, 40.0f, 0.0f),
			new Vector3 (230.0f, 20.0f, 0.0f),
			new Vector3 (130.0f, 270.0f, 0.0f),
			new Vector3 (40.0f, 160.0f, 0.0f),
			new Vector3 (180.0f,210.0f, 0.0f),
			new Vector3 (50.0f,230.0f, 0.0f),
			new Vector3 (20.0f,230.0f, 0.0f),
			new Vector3 (80.0f,130.0f, 0.0f),
			new Vector3 (70.0f,50.0f, 0.0f)
		};

		goalPoints = new Vector3[] {
			new Vector3 (0.0f, 0.0f, 0.0f),
			new Vector3 (0.0f, 0.0f, 0.0f),
			new Vector3 (0.0f, 0.0f, 0.0f),
			new Vector3 (0.0f, 0.0f, 0.0f),
			new Vector3 (0.0f, 0.0f, 0.0f),
			new Vector3 (0.0f, 0.0f, 0.0f),
			new Vector3 (0.0f, 0.0f, 0.0f),
			new Vector3 (0.0f, 0.0f, 0.0f),
			new Vector3 (0.0f, 0.0f, 0.0f),
			new Vector3 (0.0f, 0.0f, 0.0f)
		};

		Vector3 formationCenter = new Vector3 (0f, 0f, 0f);

		for (int i = 0; i < startPoints.Length; i++) {
			formationCenter += startPoints [i];
		}
		formationCenter /= startPoints.Length;

		float D = 10.0f;

		customerPoints = new Vector3[] {
			new Vector3 (formationCenter.x - D / 2, formationCenter.y + D, 0.0f),
			new Vector3 (formationCenter.x + D / 2, formationCenter.y + D, 0.0f),
			new Vector3 (formationCenter.x - D / 2, formationCenter.y, 0.0f),
			new Vector3 (formationCenter.x + D / 2, formationCenter.y, 0.0f),
			new Vector3 (formationCenter.x - 3 * D / 2, formationCenter.y, 0.0f),
			new Vector3 (formationCenter.x + 3 * D / 2, formationCenter.y, 0.0f),
			new Vector3 (formationCenter.x - D / 2, formationCenter.y - D, 0.0f),
			new Vector3 (formationCenter.x + D / 2, formationCenter.y - D, 0.0f),
			new Vector3 (formationCenter.x - 3 * D / 2, formationCenter.y - D, 0.0f),
			new Vector3 (formationCenter.x + 3 * D / 2, formationCenter.y - D, 0.0f)
		};

		map = new bool[width][];
		for (int i = 0; i < width; i++) {
			map[i] = new bool[height];
			for (int j = 0; j < height; j++) {
				map [i][j] = false;
			}
		}

		// Setting the camera
		GameObject camObj = GameObject.Find ("camera");
		camObj.transform.position = new Vector3 (width / 2, height / 2, -15.0f);
		Camera.main.orthographicSize = Mathf.Max (width, height) / 2;

		// Setting the background plane
		GameObject planeObj = GameObject.Find ("backgroundPlane");
		planeObj.transform.position = new Vector3 (width / 2, height / 2, 10.0f);

		// Drawing the startPoints
		for (int i = 0; i < startPoints.Length; i++) {
			GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
			cube.transform.position = new Vector2(startPoints[i].x, startPoints[i].y);
			cube.name = "startPoint" + "_" + i;
			cube.transform.localScale = new Vector3 (2.0f, 2.0f, 2.0f);
		}

		// Task assignment for all vehicles
		taskAssignment ();

		// PathPlanning for all points
		// Does not consider costumers now. Needs to be modified later.
		plannedPathFiltered = new List<Vector3>[startPoints.Length];
		pathFound = new bool[startPoints.Length];
		for (int i = 0; i < startPoints.Length; i++) {
			plannedPathFiltered [i] = new List<Vector3> (plannedPath [i]);
			if (plannedPath != null) {
				Debug.Log ("Path found");
				pathFound[i] = true;
				plannedPathFiltered[i] = plannedPath[i];
				int loopVar = 0;
				while (loopVar < plannedPathFiltered [i].Count - 2) {
					// plannedPath[loopVar], plannedPath[loopVar + 1], plannedPath[loopVar + 2]
					float slope1 = (((Vector3)plannedPathFiltered [i] [loopVar + 1]).y - ((Vector3)plannedPathFiltered [i] [loopVar]).y) / (float)(((Vector3)plannedPathFiltered [i] [loopVar + 1]).x - ((Vector3)plannedPathFiltered [i] [loopVar]).x);
					float slope2 = (((Vector3)plannedPathFiltered [i] [loopVar + 2]).y - ((Vector3)plannedPathFiltered [i] [loopVar + 1]).y) / (float)(((Vector3)plannedPathFiltered [i] [loopVar + 2]).x - ((Vector3)plannedPathFiltered [i] [loopVar + 1]).x);
					if (slope1 == slope2) {
						plannedPathFiltered [i].RemoveAt (loopVar + 1);
					} else {
						loopVar++;
					}
				}
			} else {
				Debug.Log ("No path found");
				pathFound[i] = false;
			}
		}

		createVehicles();
		generateFloatMap ();
	}

	void startForTasks_4 () {
		width = 100;
		height = 100;

		startPoints = new Vector3[12];
		goalPoints = new Vector3[12];

		for (int i = 0; i < 12; i++) {
			float theta = i * Mathf.PI / 12;
			startPoints [i].x = 50.0f + (30.0f * Mathf.Cos (theta));
			startPoints [i].y = 50.0f + (30.0f * Mathf.Sin (theta));
			theta += Mathf.PI;
			goalPoints [i].x = 50.0f + (30.0f * Mathf.Cos (theta));
			goalPoints [i].y = 50.0f + (30.0f * Mathf.Sin (theta));
		}

		map = new bool[width][];
		for (int i = 0; i < width; i++) {
			map[i] = new bool[height];
			for (int j = 0; j < height; j++) {
				map [i][j] = false;
			}
		}

		// Setting the camera
		GameObject camObj = GameObject.Find ("camera");
		camObj.transform.position = new Vector3 (width / 2, height / 2, -15.0f);
		Camera.main.orthographicSize = Mathf.Max (width, height) / 2;

		// Setting the background plane
		GameObject planeObj = GameObject.Find ("backgroundPlane");
		planeObj.transform.position = new Vector3 (width / 2, height / 2, 10.0f);

		// Drawing the startPoints
		for (int i = 0; i < startPoints.Length; i++) {
			GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
			cube.transform.position = new Vector2(startPoints[i].x, startPoints[i].y);
			cube.name = "startPoint" + "_" + i;
			cube.transform.localScale = new Vector3 (2.0f, 2.0f, 2.0f);
		}

		// Drawing the goalPoints
		for (int i = 0; i < goalPoints.Length; i++) {
			GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
			cube.transform.position = new Vector2(goalPoints[i].x, goalPoints[i].y);
			cube.name = "goalPoints" + "_" + i;
			cube.transform.localScale = new Vector3 (3.0f, 3.0f, 3.0f);
		}

		// PathPlanning for all points
		// Does not consider costumers now. Needs to be modified later.
		plannedPath = new List<Vector3>[startPoints.Length];
		plannedPathFiltered = new List<Vector3>[startPoints.Length];
		pathFound = new bool[startPoints.Length];
		for (int i = 0; i < startPoints.Length; i++) {
			plannedPath [i] = new List<Vector3> ();
			plannedPath [i].Add (goalPoints [i]);
			plannedPathFiltered [i] = new List<Vector3> (plannedPath [i]);
			if (plannedPath != null) {
				Debug.Log ("Path found");
				pathFound[i] = true;
				plannedPathFiltered[i] = plannedPath[i];
//				int loopVar = 0;
//				while (loopVar < plannedPathFiltered [i].Count - 2) {
//					// plannedPath[loopVar], plannedPath[loopVar + 1], plannedPath[loopVar + 2]
//					float slope1 = (((Vector3)plannedPathFiltered [i] [loopVar + 1]).y - ((Vector3)plannedPathFiltered [i] [loopVar]).y) / (float)(((Vector3)plannedPathFiltered [i] [loopVar + 1]).x - ((Vector3)plannedPathFiltered [i] [loopVar]).x);
//					float slope2 = (((Vector3)plannedPathFiltered [i] [loopVar + 2]).y - ((Vector3)plannedPathFiltered [i] [loopVar + 1]).y) / (float)(((Vector3)plannedPathFiltered [i] [loopVar + 2]).x - ((Vector3)plannedPathFiltered [i] [loopVar + 1]).x);
//					if (slope1 == slope2) {
//						plannedPathFiltered [i].RemoveAt (loopVar + 1);
//					} else {
//						loopVar++;
//					}
//				}
			} else {
				Debug.Log ("No path found");
				pathFound[i] = false;
			}
		}

		createVehicles ();
		generateFloatMap ();
	}

	void startForTasks_1_2_B () {
		width = 300;
		height = 300;
		startPoints = new Vector3[] {
			new Vector3 (30.0f, 70.0f, 0.0f),
			new Vector3 (130.0f, 40.0f, 0.0f),
			new Vector3 (230.0f, 20.0f, 0.0f),
			new Vector3 (130.0f, 270.0f, 0.0f),
			new Vector3 (40.0f, 160.0f, 0.0f),
			new Vector3 (180.0f,210.0f, 0.0f),
			new Vector3 (50.0f,230.0f, 0.0f),
			new Vector3 (20.0f,230.0f, 0.0f),
			new Vector3 (80.0f,130.0f, 0.0f),
			new Vector3 (70.0f,50.0f, 0.0f)
		};

		// Setting the camera
		GameObject camObj = GameObject.Find ("camera");
		camObj.transform.position = new Vector3 (width / 2, height / 2, -15.0f);
		Camera.main.orthographicSize = Mathf.Max (width, height) / 2;

		// Setting the background plane
		GameObject planeObj = GameObject.Find ("backgroundPlane");
		planeObj.transform.position = new Vector3 (width / 2, height / 2, 10.0f);

		// Drawing the startPoints
		for (int i = 0; i < startPoints.Length; i++) {
			GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
			cube.transform.position = new Vector2(startPoints[i].x, startPoints[i].y);
			cube.name = "startPoint" + "_" + i;
			cube.transform.localScale = new Vector3 (2.0f, 2.0f, 2.0f);
		}

		// initializing robots
		int nVehicles = startPoints.Length;
		GameObject vehicle = GameObject.Find ("Sphere");
		for (int i = 0; i < nVehicles; i++) {
			Vector3 pos = startPoints [i];
			GameObject clone = (GameObject)Instantiate (vehicle, pos, Quaternion.identity);
			clone.transform.localScale = new Vector3 (5.0f, 5.0f, 5.0f);
			clone.name = "Vehicle" + i.ToString ();
			List<Vector3> tempWaypoint = new List<Vector3> ();
			tempWaypoint.Add (new Vector3 (startPoints [i].x + 10.0f, startPoints [i].y + 10.0f, 0.0f));
			ObjectBehavior temp = new ObjectBehavior (startPoints [i], tempWaypoint, i, nVehicles);
			vehicles.Add (temp);
		}
	}

	void Start() {
		startForTasks_3_4_5 ();
		// startForTasks_1_2_A ();
		// startForTasks_1_2_B ();
		// startForTasks_4 ();
	}

	private void generateFloatMap()
	{
		floatMap = new float[width][];
		for (int i = 0; i < width; i++)
		{
			floatMap[i] = new float[height];
			for (int j = 0; j < height; j++)
			{
				if (map[i][j] == true)
					floatMap[i][j] = -2;
				else 
					floatMap[i][j] = 0;
			}
		}
	}

	public struct vehicleInfo
	{
		public Vector3 position;
		public Vector3 speed;
	}

	// Update is called once per frame
	void FixedUpdate () {
		for (int i = 0; i < vehicles.Count; i++) {
			//vehicles [i].Move_1_2 ();
			for (int k = 0; k < i; k++) {
				vehicles [i].setVehicleInformation (vehiclesInformation [k].speed, vehiclesInformation [k].position, k);
			}
			floatMap = vehicles [i].Move (floatMap);
			tempo.position = vehicles [i].getPosition ();
			tempo.speed = vehicles [i].getSpeed ();
			vehiclesInformation [i] = tempo;
		}
	}
}